var arr = [
  "Alborz",
  "Ardabil",
  "Bushehr",
  "Chaharmahal and Bakhtiari",
  "East Azerbaijan",
  "Isfahan",
  "Fars",
  "Gilan",
  "Golestan",
  "Hamadan",
  "Hormozgan",
  "Ilam",
  "Kerman",
  "Kermanshah",
  "Khuzestan",
  "Kohgiluyeh and Boyer-Ahmad",
  "Kurdistan",
  "Lorestan",
  "Markazi",
  "Mazandaran",
  "North Khorasan",
  "Qazvin",
  "Qom",
  "Razavi Khorasan",
  "Semnan",
  "Sistan and Baluchestan",
  "South Khorasan",
  "Tehran",
  "West Azerbaijan",
  "Yazd",
  "Zanjan"
];


let input = document.querySelector('#myInput');
let auto= document.querySelector('.autocomplete');
let div,notFound;

input.addEventListener('keyup' , function() {
  input.value !== "" ? auto.style.display = 'flex':auto.style.display = 'none';
  value = input.value;
  let newCity= [];
  let city,c;

  for(city of arr) {
    let exsist = true;
    
    for(c in value) {

        if(!city[c]) break

      if(city[c].toLowerCase() != value[c].toLowerCase()){ 
        exsist = false;
        break;
      }
    }

    if(exsist && input.value !== "") {
      newCity.push(city)
    }
    
  }  
  if(newCity.length){
    auto.innerHTML='';
    newCity.forEach(item => {
      div = document.createElement('div');
      div.classList.add('item');
      auto.appendChild(div);
      div.innerHTML = item;
    })
  }else{
    auto.innerHTML='';
    if(input.value !== ''){
      notFound = document.createElement('div');
      notFound.classList.add('not-found');
      auto.appendChild(notFound);
      notFound.innerHTML = 'Not Found!';
    }
  }

  let items = document.querySelectorAll('.item');
  items.forEach((item) => {
    item.addEventListener('click' , function() {
      input.value = item.innerHTML;
      auto.style.display = 'none';
      auto.innerHTML ="";
    })
  })
})

let body = document.querySelector('body');
body.addEventListener('click' , function() {
    auto.style.display = 'none';
    auto.innerHTML ="";
    

})



